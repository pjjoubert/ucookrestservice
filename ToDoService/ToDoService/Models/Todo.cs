﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace ToDoService.Models
{
    public class Todo
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string title { get; set; }
        public string summery { get; set; }
        public DateTime dateCompleted { get; set; }


    }

}