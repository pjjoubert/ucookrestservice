﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ToDoService.Models;

namespace ToDoService.Controllers
{
    public class TodosController : ApiController
    {
        private ToDoServiceContext db = new ToDoServiceContext();

        // GET: api/TodoModels
        public IQueryable<Todo> GetTodoModels()
        {
            return db.TodoModels;
        }

        // GET: api/TodoModels/5
        [ResponseType(typeof(Todo))]
        public async Task<IHttpActionResult> GetTodoModel(int id)
        {
            Todo todoModel = await db.TodoModels.FindAsync(id);
            if (todoModel == null)
            {
                return NotFound();
            }

            return Ok(todoModel);
        }

        // PUT: api/TodoModels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTodoModel(int id, Todo todoModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todoModel.Id)
            {
                return BadRequest();
            }

            db.Entry(todoModel).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TodoModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TodoModels
        [ResponseType(typeof(Todo))]
        public async Task<IHttpActionResult> PostTodoModel(Todo todoModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TodoModels.Add(todoModel);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = todoModel.Id }, todoModel);
        }

        // DELETE: api/TodoModels/5
        [ResponseType(typeof(Todo))]
        public async Task<IHttpActionResult> DeleteTodoModel(int id)
        {
            Todo todoModel = await db.TodoModels.FindAsync(id);
            if (todoModel == null)
            {
                return NotFound();
            }

            db.TodoModels.Remove(todoModel);
            await db.SaveChangesAsync();

            return Ok(todoModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TodoModelExists(int id)
        {
            return db.TodoModels.Count(e => e.Id == id) > 0;
        }
    }
}