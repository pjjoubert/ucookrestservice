namespace ToDoService.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using ToDoService.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<ToDoService.Models.ToDoServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ToDoService.Models.ToDoServiceContext";
        }

        protected override void Seed(ToDoService.Models.ToDoServiceContext context)
        {
            context.TodoModels.AddOrUpdate(x => x.Id,
                new Todo() { Id = 1, title = "First Todo", summery = "The first one to do", dateCompleted = new DateTime(2017, 11, 22) },
                new Todo() { Id = 2, title = "Second Todo", summery = "The Next one on the list", dateCompleted = new DateTime(2017, 11, 22)}
                );
        }
    }
}
